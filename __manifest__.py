{
    "name": "CG SCOP - Etablissements",
    "summary": "CG SCOP - Etablissements",
    "version": "12.0.1.0.0",
    "development_status": "Beta",
    "author": "Le Filament",
    "maintainers": ["remi-filament"],
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "cgscop_partner",
    ],
    "data": [
        "views/res_partner.xml",
    ]
}
