# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class SecondaryFacility(models.Model):
    _inherit = "res.partner"

    type = fields.Selection(
        selection_add=[('facility', 'Etablissement')])

    staff = fields.Integer("Effectif")

    facility_ids = fields.One2many(
        'res.partner', 'parent_id',
        string='Etablissements Secondaires',
        domain=[('active', '=', True), ('type', '=', 'facility')])
    other_child_ids = fields.One2many(
        'res.partner', 'parent_id', string='Autres Contacts',
        domain=[('active', '=', True), ('mandate_id', '=', False),
                ('type', '!=', 'facility')])
